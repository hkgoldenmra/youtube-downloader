主目錄 包含：
library 目錄
download.bat 檔案 (Windows 使用)
download.sh 檔案 (Linux 使用)
list.txt 檔案
readme.txt 檔案

library 目錄 包含：
yt-dlp.txt 檔案

1. library 目錄
	1.1 下載支援對應系統的 yt-dlp 主程式 (yt-dlp.txt 檔案中有註明)
	1.2 將 yt-dlp 主程式移動到 library 目錄中
2. 回到主目錄
	2.1 開啟 list.txt 檔案
		2.1.1 將需要下載的 youtube 影片的網址複製
		2.1.2 在 list.txt 檔案貼上
		2.1.3 每行一個網址
	2.2 執行 download.bat 或 download.sh 檔案
		2.2.1 如果 主目錄 沒有 download 目錄，執行程式 會自動建立 download 目錄
		2.2.2 下載的檔案會保存在 download 目錄中
		2.2.2 相同名稱的檔案會自動覆蓋
3. 備註
	3.1 預設下載 m4a 格式，是一種聲音格式，沒有影像，體積比 mp4 細，因此相同狀態下，下載時間會比較少
	3.2 如果需要下載 mp4 格式，可以到 download.bat 或 download.sh 檔案，將標示 format=m4a 的內容修改成 format=mp4